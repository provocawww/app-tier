// /pieces-situated/deck-inserter.js
// This file is only necessary because of a bug in Genie: https://github.com/GenieFramework/Genie.jl/issues/551
// Once the bug is fixed, this file can be deleted.

import * as prteDeck from '/prte-web-components/prte-deck/prte-deck.js'

let deck = document.createElement('prte-deck')
let deckTarget = document.getElementById('div_deck-target')

deckTarget.append(deck)