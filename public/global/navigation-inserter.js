// /global/navigation-inserter.js
// This file is only necessary because of a bug in Genie: https://github.com/GenieFramework/Genie.jl/issues/551
// Once the bug is fixed, this file can be deleted.
 
import * as prteNav from '/prte-web-components/prte-navigation/prte-navigation.js'

const prteNavElement = document.createElement('prte-navigation')
let layoutContainer = document.querySelector('.layout-container')

layoutContainer.prepend(prteNavElement)