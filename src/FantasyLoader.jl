module FantasyLoader
export load_fantasy_config, SparqlEndpoint
using URIs, Base64

# Matches URIs
# TODO: Add IRI support when Julia devs fix the Unicode ranges bug (currently using workaround with ugly, invisible Unicode characters directly inserted into regex)
const URI_REGEX = r"(?<IRI>(?<scheme>[A-Za-z][A-Za-z\+\-]+)\Q://\E(?<host>(?>\w[A-Za-z0-9\-\.]*\.)?[A-Za-z]+)(?>\:(?<port>\d*))?(?<path>(?>\/(?<unit>[A-Za-z0-9¡-󠇯\Q!$&'()*+,-.:;=@_~\E]+)?)*)(?<query>\?(?&unit)*)?(?<fragment>\#(?&unit)*)?)"m

# 
const FANT_REGEX = r"(?<property>[a-z]*\:[a-z]*\_IRI)\s+\"" * URI_REGEX * r"\"\Q^^xsd:anyURI\E"m
const USERNAME_REGEX = r"""\Qfant:http_basic_username \E[\"\'](?<username>.*)[\"\']"""m
const PASSWORD_REGEX = r"""\Qfant:http_basic_password \E[\"\'](?<password>.*)[\"\']"""m

abstract type AuthoCreds end
struct HttpBasicCreds <: AuthoCreds
	username::String
	password::String
	autho_header::String
	function HttpBasicCreds(username::AbstractString, password::AbstractString)
		return new(
			String(username),
			String(password),
			"Basic " * base64encode(username * ':' * password)
		)
	end
end
struct SparqlEndpoint
	db_IRI::URI
	query_IRI::URI
	update_IRI::URI
	autho_creds::AuthoCreds
end

"""
	load_fantasy_config(path::AbstractString)

Loads the configuration file at `path`. The config should be a `.trig` file written according to the `secrets-ontology.trig` file in the Content Pipeline repo.

Unlike the content pipeline, this function depends on regular expressions. It does not understand RDF semantically. Do not change the prefixes or format of any `secrets.trig` files (e.g. do not attempt to convert them to N-Quads or change the `fant` prefix to something else).
"""

function load_fantasy_config(path::AbstractString)
	if !isfile(path)
		error("did not find a config file at path \"$(path)\"")
	end
	config_markup = read(path, String)

	# Select the username and password from the config file
	username = match(USERNAME_REGEX, config_markup)[:username]
	password = match(PASSWORD_REGEX, config_markup)[:password]

	# Select the IRIs that correspond to the properties `fant:db_IRI`, `fant:query_IRI`, and `fant:update_IRI`
	iri_matches = collect( eachmatch(FANT_REGEX, config_markup) )
	db_IRI = URI( filter(match -> match["property"] == "fant:db_IRI", iri_matches)[1][:IRI] )
	query_IRI = URI( filter(match -> match["property"] == "fant:query_IRI", iri_matches)[1][:IRI] )
	update_IRI = URI( filter(match -> match["property"] == "fant:update_IRI", iri_matches)[1][:IRI] )
	return SparqlEndpoint(db_IRI, query_IRI, update_IRI, HttpBasicCreds(username, password))
end

end