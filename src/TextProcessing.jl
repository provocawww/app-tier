module TextProcessing
export minify_SPARQL

"""
	minify_SPARQL(query_string::AbstractString)

	Removes excess tabs and whitespace from SPARQL queries. Not the nicest minifier ever, but it reduces the number of unnecessary characters.
"""
function minify_SPARQL(query_string::AbstractString)
	iter1 = replace(query_string, r"\n\h*" => "\n") # Remove tabs
	iter2 = replace(iter1, # Remove unnecessary line breaks
		r"\.\n" => ".",
		r"\;\n" => ";",
		r"\{\n" => "{",
		r"\}\n" => "}"
	)
	return iter2
end

end