using Pkg, Logging

using Genie

function quit()
	@info "Quitting ProvocaWWW…"
	Genie.down()
end

atexit(quit)
Genie.loadapp(".")
up(33204)
wait(Condition())