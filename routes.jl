using Genie.Renderer.Html,
	Genie.Requests,
	Genie.Router,
	HTTP,
	ProvocaWWWAppTier.DeckController,
	ProvocaWWWAppTier.PiecesController,
	ProvocaWWWAppTier.TheoryTypes,
	TimeZones,
	URIs

route("/", named = :index) do
	serve_static_file("index.html")
end

route("/pieces", named = :pieces) do
	html(
		:pieces, # resource
		:browse_pieces, # view
		layout = :app,
		title = "Recent work",
		subtitle = "A list of the most recent ProvocaTeach creations."
	)
end

route("/pieces/:blog_title") do
	slug = params(:blog_title)
	render_coverpage(slug, SPARQL_ENDPT)
end

# Blog access API

route("/api/pieces/deck/new", named = :piece_cards) do 
	offset = getpayload(:offset, 0)
	limit = getpayload(:limit, 10)
	render_piecedeck_data(offset, limit, SPARQL_ENDPT)
end
