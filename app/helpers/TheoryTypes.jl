module TheoryTypes
using JSON3, URIs, Dates, TimeZones

export Piece, Page, CoverPage, Date_Hour_Minute, User_Date_Format, page_title, piece_title, page_heart, creator_name, user_date_str

"""
		Person(preferred_name::String)

A person.
"""

abstract type Page end

struct Person
	preferred_name::String
end

abstract type DigitalFile end

struct ImageFile <: DigitalFile
	content_IRI::URI
	alt_text::String
end
struct CoverPage <: Page
	page_title::String
	piece_title::String
	piece_slug::String
	piece_subtitle::String
	date_made_public::ZonedDateTime
	page_heart::String
	creator_name::String
	page_thumbnail::ImageFile
end

const Date_Hour_Minute = Dates.DateFormat("yyyy-mm-ddTHH:MM:SSzzzz")

const User_Date_Format = Dates.DateFormat("yyyy U d")

"""
Convenience method for when the date is given as a string of the form "yyyy-mm-ddTHH:MM:SSzzzz", e.g. `"2022-04-28T15:27:00+06:00`.
"""
function CoverPage(
	page_title::String,
	piece_title::String,
	piece_slug::String,
	piece_subtitle::String,
	date_string::String,
	page_heart::String,
	creator_name::String,
	page_thumbnail::ImageFile
)
	date_made_public = ZonedDateTime(date_string, Date_Hour_Minute)
	return CoverPage(page_title, piece_title, piece_slug, piece_subtitle, date_made_public, page_heart, creator_name, page_thumbnail)
end

struct Piece
	creator::Union{Person, Nothing}
	piece_title::String
	cover_page::Page
	inner_pages::Union{AbstractArray{Page}, Nothing}
	medium::Symbol
end

function user_date_str(coverpage::CoverPage)
	Dates.format(coverpage.date_made_public, User_Date_Format)
end

end