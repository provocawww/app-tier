module PiecesController
using Genie, Genie.Renderer, Genie.Renderer.Html
using HTTP, URIs, JSON3, Base64, Dates, TimeZones
using ProvocaWWWAppTier.TheoryTypes, ProvocaWWWAppTier.FantasyLoader

export construct_coverpage_data, select_coverpage_data, render_coverpage, render_browse_pieces, prte__navigation

Genie.Renderer.Html.register_normal_element("prte__navigation", context = PiecesController)

const PREFIXES = join([
	"PREFIX theo: <https://rdf.provocateach.art/theory-ontology#>",
	"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>",
	"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>",
	"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>",
	"PREFIX sh: <http://www.w3.org/ns/shacl#>"
], "\n")::String
"""
select_coverpage_data(slug::AbstractString, sparql_endpoint::SparqlEndpoint)

Given the `slug` of a blog piece and the URI of a SPARQL endpoint, returns a `CoverPage` object containing the blog’s title, parent piece title, creator name, and main content (or “heart” of the piece.)
"""
function select_coverpage_data(slug::AbstractString, sparql_endpoint::SparqlEndpoint)
	headers = Dict(
		"Accept" => "application/sparql-results+json",
		"Authorization" => sparql_endpoint.autho_creds.autho_header
	)
	response = HTTP.get(
		sparql_endpoint.query_IRI;
		headers = headers,
		query = "query=" * URIs.escapeuri(join([
			# TODO: Refactor with `minify_SPARQL`
				PREFIXES,
				"SELECT ?piece_title ?piece_subtitle ?date_made_public ?page_title ?creator_name ?page_heart ?image_IRI ?alt_text",
				"WHERE {",
					"GRAPH ?g1 {",
						"?blog_piece",
							"a theo:Content_Piece ;",
							"theo:piece_slug \"$(slug)\"^^xsd:token ;",
							"theo:piece_title ?piece_title ;",
							"theo:piece_subtitle ?piece_subtitle ;",
							"theo:date_made_public ?date_made_public ;",
							"theo:medium theo:blog ;",
							"theo:creator ?creator ;",
							"theo:has_cover_page ?cover_page .",
						"?cover_page",
							"a theo:Cover_Page ;",
							"theo:page_title ?page_title ;",
							"theo:page_heart ?page_heart .",
						"?image",
							"a theo:Image_File ;",
							"theo:content_IRI ?image_IRI ;",
							"theo:alt_text ?alt_text .",
					"}",
					"GRAPH ?g2 {",
						"?creator",
							"a theo:Person ;",
							"theo:preferred_name ?creator_name .",
					"}",
				"}"
		], '\n'))
	)
	json = JSON3.read( String(response.body) )
	coverpage_obj = json[:results][:bindings][1] # Must reach deep into the JSON for the important info
	return TheoryTypes.CoverPage(
		coverpage_obj[:page_title][:value],
		coverpage_obj[:piece_title][:value],
		slug,
		coverpage_obj[:piece_subtitle][:value],
		ZonedDateTime(coverpage_obj[:date_made_public][:value], TheoryTypes.Date_Hour_Minute),
		coverpage_obj[:page_heart][:value],
		coverpage_obj[:creator_name][:value],
		TheoryTypes.ImageFile(
			URIs.URI(coverpage_obj[:image_IRI][:value]),
			coverpage_obj[:alt_text][:value]
		)
	)
end

function render_coverpage(coverpage::CoverPage)
	html(
		:pieces, # resource
		:blog_piece, # view
		coverpage = coverpage, # used when rendering the view
		date_str = user_date_str(coverpage),
		layout = :coverpage
	)
end

function render_coverpage(slug::AbstractString, db_uri)
	coverpage = select_coverpage_data(slug, db_uri)
	render_coverpage(coverpage)
end

end