module DeckController
using ProvocaWWWAppTier.TextProcessing, ProvocaWWWAppTier.FantasyLoader
using HTTP, URIs, JSON3

export select_piecedeck_data, render_piecedeck_data

"""
    select_piecedeck_data(offset::Integer, limit::Integer)

Returns JSON containing the title, slug, subtitle, date made public, creator name, image IRI, and image alt text
"""
function select_piecedeck_data(offset::Integer, limit::Integer, sparql_endpoint::SparqlEndpoint)
	query_string = minify_SPARQL("""
		PREFIX theo: <https://rdf.provocateach.art/theory-ontology#>
		PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
		SELECT ?piece_title ?piece_slug ?piece_subtitle ?date_made_public ?creator_name ?image_IRI ?image_alt_text
		WHERE {
			GRAPH ?g1 {
				?blog_piece
					a theo:Content_Piece ;
					theo:piece_slug ?piece_slug ;
					theo:piece_title ?piece_title ;
					theo:piece_subtitle ?piece_subtitle ;
					theo:date_made_public ?date_made_public ;
					theo:medium theo:blog ;
					theo:creator ?creator ;
					theo:has_cover_page ?cover_page .
				?cover_page
					a theo:Cover_Page ;
					theo:page_title ?page_title .
				?image
					a theo:Image_File ;
					theo:content_IRI ?image_IRI ;
					theo:alt_text ?image_alt_text .
			}
			GRAPH ?g2 {
				?creator
					a theo:Person ;
					theo:preferred_name ?creator_name .
			}
			FILTER (?date_made_public < NOW())
		}
		ORDER BY DESC(?date_made_public)
		OFFSET $(offset)
		LIMIT $(limit)
	""")
	headers = Dict(
		"Accept" => "application/sparql-results+json",
		"Authorization" => sparql_endpoint.autho_creds.autho_header
	)
	response = HTTP.get(
		sparql_endpoint.query_IRI;
		headers = headers,
		query = "query=" * URIs.escapeuri(query_string)
	)
	json = JSON3.read( String(response.body) )
	pieces = json[:results][:bindings]
	return pieces
end

function render_piecedeck_data(offset::Integer, limit::Integer, sparql_endpoint::SparqlEndpoint)
	body_str = JSON3.write( select_piecedeck_data(offset, limit, sparql_endpoint) )
	return HTTP.Response(
		200,
		[
			("Content-Type", "application/json"),
			("Cache-Control", "no-cache")
		],
		body = body_str
	)
end

end
