using Genie.Configuration, Logging
using ProvocaWWWAppTier.FantasyLoader
import URIs

const SPARQL_ENDPT = load_fantasy_config("config/development-secrets.trig")

const config = Settings(
	server_port                     = 33204,
	server_host                     = "127.0.0.1",
	log_level                       = Logging.Debug,
	log_to_file                     = false,
	server_handle_static_files      = true
)

ENV["JULIA_REVISE"] = "auto"