using Genie.Configuration, Logging
using ProvocaWWWAppTier.FantasyLoader

const SPARQL_ENDPT = load_fantasy_config("config/readonly-production-secrets.trig")

const config = Settings(
  server_port                     = 33204,
  server_host                     = "0.0.0.0",
  log_level                       = Logging.Error,
  log_to_file                     = true,
  server_handle_static_files      = false
)

if config.server_handle_static_files
  @warn("For performance reasons Genie should not serve static files (.css, .js, .jpg, .png, etc) in production.
         It is recommended to set up Apache or Nginx as a reverse proxy and cache to serve static assets.")
end

ENV["JULIA_REVISE"] = "off"